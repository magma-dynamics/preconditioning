To generate C++ code from the UFL files:

    ffc -l dolfin -O -f split -r quadrature *.ufl
