// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <limits>
#include <petsc.h>
#include <dolfin.h>

#include "../../common/create_field_split.h"
#include "../../common/parameters.h"
#include "../../common/linear_solver.h"
#include "../../common/set_nullspace.h"

#include "../../forms/Compaction3D.h"
#include "../../forms/Compaction3D_PC.h"
#include "../../forms/Porosity.h"
#include "../../forms/Magma_u.h"

#if defined(HAS_PETSC)

//*************************************************************************
// Given porosity field
class givenPorosity : public dolfin::Expression
{
public:
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double xx = x[0];
    double yy = x[1];
    double zz = x[2];
    double xc = 0.3;         // centre of gaussian
    double yc = 0.5;
    double zc = 1.0-xc;
    double omega_max = 0.07; // max standard deviation
    double omega_min = 0.01; // min standard deviation
    double omega = ((omega_max-omega_min)/(zc - 1.0))*(zz-1.0) + omega_min;
    double phimax = 0.2;
    double phimin = 0.0;
    double phi = (phimax - phimin)*exp(-((xx - xc)*(xx - xc) 
		    + (yy - yc)*(yy - yc))/(2.0*omega*omega)) + phimin;
    values[0] = phi;
  }
};

//***************************************************************************
// Define boundaries
class Gamma0 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
    return (std::abs(x[0] + x[2] - 1.0) < DOLFIN_EPS && x[0] > 0.1 && on_boundary);
  }
};

class Gamma1 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[2] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class PinPoint : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
    return std::abs(x[0]-1.5) < DOLFIN_EPS && x[1] < DOLFIN_EPS && x[2] < DOLFIN_EPS;
  }
};

//***************************************************************************
// Permeability
class kk : public dolfin::Expression
{
public:
  kk(double R, double rzeta, double phi0) : R(R), rzeta(rzeta), phi0(phi0) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = (*pphi)(x[0], x[1], x[2]);
    double kk  = (phi / phi0) * (phi / phi0);
    values[0]  = (R*R/(rzeta + 4./3.)) * kk;
  }
  boost::shared_ptr<dolfin::Function> pphi;

private:
  const double R, rzeta, phi0;
};

// Permeability divided by phi
class kkphi : public dolfin::Expression
{
public:
  kkphi(double R, double rzeta, double phi0) : R(R), rzeta(rzeta), phi0(phi0) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = (*pphi)(x[0], x[1], x[2]);
    double kk  = (phi / phi0) * (1.0 / phi0);
    values[0]  = (R*R/(rzeta + 4./3.)) * kk;
  }
  boost::shared_ptr<dolfin::Function> pphi;

private:
  const double R, rzeta, phi0;
};

class zetam1 : public dolfin::Expression
{
public:
  zetam1(double phi0, double rzeta) : phi0(phi0), rzeta(rzeta) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = (*pphi)(x[0], x[1], x[2]);
    double zetainverse = (1.0 / rzeta) * (phi / phi0);
    double zetacut = 1.e-4;
    double nzetainverse = zetainverse;
    if (zetainverse < zetacut){
      nzetainverse = zetacut;
    }
    values[0] = nzetainverse;
  }
  boost::shared_ptr<dolfin::Function> pphi;

private:
  const double phi0, rzeta;
};

class eta : public dolfin::Expression
{
public:
  eta(double lambda, double phi0) : lambda(lambda), phi0(phi0) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = (*pphi)(x[0], x[1], x[2]);
    double eta = exp(-lambda*(phi - phi0));
    values[0] = 2.0*eta;
  }
  boost::shared_ptr<dolfin::Function> pphi;

private:
  const double lambda, phi0;
};

class P22factor : public dolfin::Expression
{
public:
  P22factor(double lambda, double phi0) : lambda(lambda), phi0(phi0) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double phi = (*pphi)(x[0], x[1], x[2]);
    const double eta = 2.0*exp(-lambda*(phi - phi0));
    values[0] = tanh(1.0/eta);
  }
  boost::shared_ptr<dolfin::Function> pphi;

private:
  const double lambda, phi0;
};

//***************************************************************************
// Source term
class Source : public dolfin::Expression
{
public:
  Source() : Expression(3) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double phi = (*pphi)(x[0], x[1], x[2]);
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = phi;
  }
  boost::shared_ptr<dolfin::Function> pphi;
};

//***************************************************************************
// Solve for the system for fluid velocity

void petsc_uf(dolfin::PETScMatrix& A, dolfin::PETScVector& b,
              dolfin::Function& u)
{
  KSP ksp_uf; PC pc_uf;
  KSPCreate(PETSC_COMM_SELF,&ksp_uf);
  KSPSetOptionsPrefix(ksp_uf,"uf_");

  KSPSetOperators(ksp_uf, A.mat(), A.mat());
  KSPGetPC(ksp_uf,&pc_uf);

  KSPSetType(ksp_uf,KSPGMRES);
  PCSetType(pc_uf,PCILU);

  KSPSetFromOptions(ksp_uf);
  KSPSetUp(ksp_uf);
  dolfin::PETScVector x;
  x.init(MPI_COMM_WORLD, A.size(1));
  KSPSolve(ksp_uf, b.vec(), x.vec());

  KSPConvergedReason reason;
  KSPGetConvergedReason(ksp_uf, &reason);
  PetscInt its;
  KSPGetIterationNumber(ksp_uf, &its);
  PetscReal rnorm;
  KSPGetResidualNorm(ksp_uf, &rnorm);

  std::cout << "----\n";
  if(reason < 0)
  {
    std::cout << "\t\tPetSc: Magma velocity. Solving the linear system "
	      << "has failed. Reason code: "
	      << reason << std::endl
	      << "Check KSPConvergedReason for the reason"
	      << std::endl
	      << "\t\tPetSc: Residual after "
	      << int(its) << " iterations : ";
    std::cout << rnorm << std::endl;
  }
  else
  {
    if (reason > 0)
    {
      std::cout << "\t\tPetSc: Magma velocity. Solving the linear system "
		<< "has succeeded. Reason code: "
		<< reason << std::endl
		<< "\t\tPetsc: Convergence in "
		<< int(its) << " iterations : ";
      std::cout << rnorm << std::endl;
    }
  }

  (*u.vector()) = x;
  KSPDestroy(&ksp_uf);
}

//***************************************************************************
int main(int argc, char* argv[])
{
  // Allow extrapolation
  dolfin::parameters["allow_extrapolation"] = true;

  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Organise model parameters
  const dolfin::Constant phi0(parameters["phi0"]);
  const double rzeta = 5./3.;
  const double lambda = 27.0;
  const double R = 0.1;

  // Mesh
  const std::string mesh_file = parameters["mesh_file"];
  dolfin::Mesh mesh(mesh_file);

  // Function spaces
  Compaction3D::FunctionSpace W(mesh);
  dolfin::cout << "Number of degrees of freedom: " << W.dim() << dolfin::endl;
  dolfin::SubSpace W0(W, 0);
  dolfin::Function w(W);
  dolfin::Function uu = w[0];
  dolfin::Function pf = w[1];
  dolfin::Function pc = w[2];

  Porosity::FunctionSpace Q(mesh);
  dolfin::Function phi(Q);
  givenPorosity givenPhi;
  phi = givenPhi;

  // porosity shared pointer
  boost::shared_ptr<dolfin::Function> spphi(new dolfin::Function(phi));

  // Set-up boundary conditions
  Gamma0 g0;
  Gamma1 g1;
  const dolfin::Constant u_slope(1.0/sqrt(2.0), 0.1, -1.0/sqrt(2.0));
  const dolfin::Constant zero_vector(0.0, 0.0, 0.0);
  dolfin::DirichletBC bc0(W0, u_slope    , g0);
  dolfin::DirichletBC bc1(W0, zero_vector, g1);
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc0);
  bcs.push_back(&bc1);

  // Coefficient functions
  Source mySource;
  eta myEta(lambda, phi0);
  kk  myK(R, rzeta, phi0);
  zetam1 myZetam1(phi0, rzeta);
  mySource.pphi = spphi;
  myEta.pphi    = spphi;
  myK.pphi      = spphi;
  myZetam1.pphi = spphi;
  dolfin::Constant f(0.0, 0.0, 1.0);
  dolfin::Constant fp(0.0);

  // Create forms
  Compaction3D::BilinearForm a(W, W);
  Compaction3D::LinearForm L(W);
  L.source = mySource;
  L.f      = f;
  L.fp     = fp;
  L.kk     = myK;
  a.eta    = myEta;
  a.kk     = myK;
  a.zetam1 = myZetam1;

  // Preconditioner form
  Compaction3D_PC::BilinearForm a_P(W, W);
  P22factor myP22fac(lambda, phi0);
  myP22fac.pphi = spphi;
  dolfin::Constant sgn(1.0);
  const std::string block_pc_type = parameters["block_pc_type"];
  if (block_pc_type == "triangular")
    sgn = -1.0;

  a_P.eta    = myEta;
  a_P.kk     = myK;
  a_P.zetam1 = myZetam1;
  a_P.p22fac = myP22fac;
  a_P.sgn    = sgn;

  // Solve problem
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
	parameters["gmres_restart"], parameters["block_pc_type"],
	parameters["ksp_solver_type"], parameters["tol"], w);

  // Split solution
  dolfin::Function uu_deep = w[0];
  dolfin::Function pf_deep = w[1];
  dolfin::Function pc_deep = w[2];
  uu = uu_deep;
  pf = pf_deep;
  pc = pc_deep;

  // Compute magma velocity
  Magma_u::FunctionSpace VM(mesh);
  Magma_u::BilinearForm a_uf(VM, VM);
  Magma_u::LinearForm L_uf(VM);
  kkphi myKphi(R, rzeta, phi0);
  myKphi.pphi = spphi;
  L_uf.u      = uu;
  L_uf.kkphi  = myKphi;
  L_uf.p      = pf;
  L_uf.f      = f;
  dolfin::Function uf(VM);
  dolfin::PETScMatrix Auf; dolfin::PETScVector buf;
  assemble_system(Auf, buf, a_uf, L_uf);
  petsc_uf(Auf, buf, uf);

  // Save solution in VTK format
  dolfin::File ufile_pvd("velocity.pvd");
  dolfin::File uffile_pvd("velocity_uf.pvd");
  dolfin::File pffile_pvd("pf_pressure.pvd");
  dolfin::File pcfile_pvd("pc_pressure.pvd");
  dolfin::File phifile_pvd("porosity.pvd");

  // Write to files
  ufile_pvd << uu;
  uffile_pvd << uf;
  pffile_pvd << pf;
  pcfile_pvd << pc;
  phifile_pvd << phi;
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
