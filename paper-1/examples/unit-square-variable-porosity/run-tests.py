# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(n, phimin, phi0, solver, num_smoother, pc_smoother,
               gmres_restart, tol, bpc_type, ksp_type):
    """Run solver with given command line options and return output"""
    print "Command:", "./compaction-var-porosity" + ' --n=' + str(n) \
                                                 + ' --phimin=' + str(phimin) \
                                                 + ' --phi0=' + str(phi0) \
                                                 + ' --solver=' + solver \
                                                 + ' --amg_num_smoother=' + str(num_smoother) \
                                                 + ' --amg_smoother_pc=' + pc_smoother \
                                                 + ' --gmres_restart=' + str(gmres_restart) \
                                                 + ' --block_pc_type=' + str(bpc_type) \
                                                 + ' --ksp_solver_type=' + str(ksp_type) \
                                                 + ' --tol=' + str(tol)
    return subprocess.check_output(['./compaction-var-porosity', \
                                    '--n=' + str(n) , \
                                    '--phimin='  + str(phimin),\
                                    '--phi0='  + str(phi0),\
                                    '--solver=' + solver, \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother, \
                                    '--gmres_restart=' + str(gmres_restart), \
                                    '--block_pc_type=' + str(bpc_type), \
                                    '--ksp_solver_type=' + str(ksp_type), \
                                    '--tol=' + str(tol)])


def table_6_4_5_6():

    # Table data
    meshes = [32, 64, 128, 256]
    linearsolvers = ["minres", "bicgstab", "gmres"]
    solvers = ["lu", "amg"]
    phimins = [1.0e-3, 1.0e-5, 0]

    f = open('table-6-4-5-6-data.txt', 'w')
    data = dict()
    for linsol in linearsolvers:
        data[linsol] = dict()
        if linsol == "minres":
            bpc_type = "diagonal"
        else:
            bpc_type = "triangular"
        for mesh in meshes:
            data[linsol][mesh] = dict()
            for solver in solvers:
                data[linsol][mesh][solver] = dict()
                for phimin in phimins:
                    num_smoother = 4
                    pc_smoother = "sor"
                    tol=1.0e-10
                    out = run_solver(n=mesh, solver=solver, phimin=phimin, \
                                         phi0=0.05, num_smoother=num_smoother, \
                                         pc_smoother=pc_smoother, \
                                         gmres_restart=100, tol=tol, \
                                         bpc_type=bpc_type, \
                                         ksp_type=linsol)

                    # Parse output to get iteration count
                    data[linsol][mesh][solver][phimin] = get_iteration_count(out)

                    print out
                    f.write(out)

    # Pretty-print table
    for linsol in linearsolvers:
        table = linsol + "\n"
        table += "N".ljust(10)
        for phimin in phimins:
            for solver in solvers:
                table += ("(" + "{0:g}".format(phimin) + ", " + solver + ")").ljust(15)
        table += "\n"
        for mesh in meshes:
            table += str(mesh).ljust(10)
            for phimin in phimins:
                for solver in solvers:
                    table += str(data[linsol][mesh][solver][phimin]).ljust(15)
            table += "\n"
        print table
        f.write(table)

    # Close file
    del f

# Run Table 6.4, 6.5 and 6.6
table_6_4_5_6()
