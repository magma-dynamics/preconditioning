// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <limits>
#include <petsc.h>
#include <dolfin.h>

#include "../../common/create_field_split.h"
#include "../../common/parameters.h"
#include "../../common/linear_solver.h"
#include "../../common/set_nullspace.h"

#include "../../forms/Compaction2D.h"
#include "../../forms/Compaction2D_PC.h"
#include "../../forms/Error_sol.h"
#include "../../forms/PressureMean.h"

#if defined(HAS_PETSC)

//***************************************************************************
// Define boundary domain
class Boundary : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return on_boundary; }
};

//***************************************************************************
// Boundary solution
class Inflow : public dolfin::Expression
{
public:
  Inflow(double R, double rzeta, double phi0, double c1, double c2)
    : Expression(2), R(R), rzeta(rzeta), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double kk = (R*R/(rzeta + 4./3.)) * (phi / phi0) * (phi / phi0);
    const double dpdx = 4*DOLFIN_PI*sin(4*DOLFIN_PI*x[0])*cos(2*DOLFIN_PI*x[1]);
    const double dpdy = 2*DOLFIN_PI*cos(4*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1]);
    const double w1   = sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]) + 2.0;
    const double w2   = 0.5*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdx + w1;
    values[1] = kk*dpdy + w2;
  }

private:
  const double R, rzeta, phi0, c1, c2;
};

//***************************************************************************
// Exact solutions
class Exact_u : public dolfin::Expression
{
public:
  Exact_u(double R, double rzeta, double phi0, double c1, double c2)
    : R(R), rzeta(rzeta), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double kk = (R*R/(rzeta + 4./3.)) * (phi / phi0) * (phi / phi0);
    const double dpdx = 4*DOLFIN_PI*sin(4*DOLFIN_PI*x[0])*cos(2*DOLFIN_PI*x[1]);
    const double w1 = sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdx + w1;
  }

private:
  const double R, rzeta, phi0, c1, c2;
};

class Exact_v : public dolfin::Expression
{
public:
  Exact_v(double R, double rzeta, double phi0, double c1, double c2)
    : R(R), rzeta(rzeta), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double kk = (R*R/(rzeta + 4./3.)) * (phi / phi0) * (phi / phi0);
    const double dpdy = 2*DOLFIN_PI*cos(4*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1]);
    const double w2 = 0.5*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdy + w2;
  }

private:
  const double R, rzeta, phi0, c1, c2;
};

class Exact_p : public dolfin::Expression
{
public:
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = (-cos(4*DOLFIN_PI*x[0]))*cos(2*DOLFIN_PI*x[1]);
  }
};

//***************************************************************************
// Permeability
class kk : public dolfin::Expression
{
public:
  kk(double R, double rzeta, double phi0, double c1, double c2)
    : R(R), rzeta(rzeta), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double kk = (phi / phi0) * (phi / phi0);
    values[0] = (R*R/(rzeta + 4./3.)) * kk;
  }

private:
  const double R, rzeta, phi0, c1, c2;
};

class zetam1 : public dolfin::Expression
{
public:
  zetam1(double phi0, double rzeta, double c1, double c2)
    : phi0(phi0), rzeta(rzeta), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double zetainverse = phi / phi0;
    values[0] = (1.0 / rzeta) * zetainverse;
  }

private:
  const double phi0, rzeta, c1, c2;
};

class eta : public dolfin::Expression
{
public:
  eta(double lambda, double phi0, double c1, double c2)
    : lambda(lambda), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double eta = exp(-lambda*(phi - phi0));
    values[0] = 2.0*eta;
  }

private:
  const double lambda, phi0, c1, c2;
};

class P22factor : public dolfin::Expression
{
public:
  P22factor(double lambda, double phi0, double c1, double c2)
    : lambda(lambda), phi0(phi0), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double phi = c1 + c2*cos(4.0*DOLFIN_PI*(x[0]*sin(DOLFIN_PI/6.0) + x[1]*cos(DOLFIN_PI/6.0)));
    double eta = 2.0*exp(-lambda*(phi - phi0));
    values[0] = tanh(1.0/eta);
  }

private:
  const double lambda, phi0, c1, c2;
};

//***************************************************************************
// Source term
class Source : public dolfin::Expression
{
public:
  Source(double R, double rzeta, double phi0, double lambda, double c1, double c2)
    : Expression(2), R(R), rzeta(rzeta), phi0(phi0), lambda(lambda), c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double pi3 = DOLFIN_PI*DOLFIN_PI*DOLFIN_PI;
    const double pi2 = DOLFIN_PI*DOLFIN_PI;
    const double pi1 = DOLFIN_PI;
    const double R2  = R*R;
    const double phi02 = phi0*phi0;
    const double c12 = c1*c1;
    const double c22 = c2*c2;
    const double c23 = c2*c2*c2;

    values[0] = (exp(-c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])-c1*lambda)*(((16*sqrt(3)*pi3*c23*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])+416*pi3*c23*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1]))*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+(16*sqrt(3)*pi3*c1*c22*R2*lambda-16*sqrt(3)*pi3*c22*R2)*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])+(416*pi3*c1*c22*R2*lambda-416*pi3*c22*R2)*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1]))*sin(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])*sin(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+((32*3.0*sqrt(3.0)*pi3*c23*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])-112*pi3*c23*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1]))*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+((64*3.0*sqrt(3.0)*pi3*c1*c22*R2*lambda-224*sqrt(3)*pi3*c22*R2)*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])+(528*pi3*c22*R2-224*pi3*c1*c22*R2*lambda)*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1]))*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+(-32*3.0*sqrt(3.0)*pi3*c2*phi0*rzeta*R2*sin(4*pi1*x[0])*exp(c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+c1*lambda)+(32*3.0*sqrt(3.0)*pi3*c12*c2*R2*lambda-224*sqrt(3)*pi3*c1*c2*R2)*exp(phi0*lambda)*sin(4*pi1*x[0])+(-12*pi2*c2*phi02*rzeta-16*pi2*c2*phi02)*lambda*exp(phi0*lambda)*cos(pi1*x[0]))*sin(2*pi1*x[1])+(312*pi3*c2*phi0*rzeta*R2*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+c1*lambda)+(528*pi3*c1*c2*R2-112*pi3*c12*c2*R2*lambda)*exp(phi0*lambda)*cos(4*pi1*x[0])+(-3*3*sqrt(3)*pi2*c2*phi02*rzeta-4*3.0*sqrt(3.0)*pi2*c2*phi02)*lambda*exp(phi0*lambda)*sin(pi1*x[0]))*cos(2*pi1*x[1]))*sin(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+(16*sqrt(3)*pi3*c22*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])+736*pi3*c22*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1]))*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+((16*3.0*sqrt(3.0)*pi3*c2*phi0*rzeta*R2*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+c1*lambda)+16*sqrt(3)*pi3*c1*c2*R2*exp(phi0*lambda)*cos(4*pi1*x[0]))*sin(2*pi1*x[1])+(336*pi3*c2*phi0*rzeta*R2*sin(4*pi1*x[0])*exp(c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+c1*lambda)+1056*pi3*c1*c2*R2*exp(phi0*lambda)*sin(4*pi1*x[0]))*cos(2*pi1*x[1]))*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+(15*pi2*phi02*rzeta+20*pi2*phi02)*exp(phi0*lambda)*sin(pi1*x[0])*sin(2*pi1*x[1])+((240*pi3*c1*phi0*rzeta*R2+12*pi1*phi02*rzeta+16*pi1*phi02)*sin(4*pi1*x[0])*exp(c2*lambda*cos(2*sqrt(3)*pi1*x[1]+2*pi1*x[0])+c1*lambda)+320*pi3*c12*R2*exp(phi0*lambda)*sin(4*pi1*x[0]))*cos(2*pi1*x[1])))/(3*phi02*rzeta+4*phi02);

    values[1] = (pi1*exp(-c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))-c1*lambda)*(480*pi2*c23*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c23*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+480*pi2*c1*c22*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-480*pi2*c22*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c1*c22*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-64*sqrt(3)*pi2*c22*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+192*pi2*c23*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c23*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+384*pi2*c1*c22*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-448*pi2*c22*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+128*sqrt(3)*pi2*c1*c22*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+128*3.0*sqrt(3.0)*pi2*c22*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-192*pi2*c2*phi0*rzeta*R2*sin(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+192*pi2*c12*c2*R2*lambda*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-448*pi2*c1*c2*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+8*3.0*sqrt(3.0)*pi1*c2*phi02*rzeta*lambda*exp(phi0*lambda)*cos(pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+32*sqrt(3)*pi1*c2*phi02*lambda*exp(phi0*lambda)*cos(pi1*x[0])*sin(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+112*3.0*sqrt(3.0)*pi2*c2*phi0*rzeta*R2*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c12*c2*R2*lambda*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+128*3.0*sqrt(3.0)*pi2*c1*c2*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-18*pi1*c2*phi02*rzeta*lambda*exp(phi0*lambda)*sin(pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))-24*pi1*c2*phi02*lambda*exp(phi0*lambda)*sin(pi1*x[0])*cos(2*pi1*x[1])*sin(2*pi1*(sqrt(3)*x[1]+x[0]))+800*pi2*c22*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c22*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+528*pi2*c2*phi0*rzeta*R2*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+1120*pi2*c1*c2*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+64*3.0*sqrt(3.0)*pi2*c2*phi0*rzeta*R2*sin(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+64*sqrt(3)*pi2*c1*c2*R2*exp(phi0*lambda)*sin(4*pi1*x[0])*cos(2*pi1*x[1])*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+240*pi2*c1*phi0*rzeta*R2*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*sin(2*pi1*x[1])+12*phi02*rzeta*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*sin(2*pi1*x[1])+16*phi02*cos(4*pi1*x[0])*exp(c2*lambda*cos(2*pi1*(sqrt(3)*x[1]+x[0]))+c1*lambda)*sin(2*pi1*x[1])+320*pi2*c12*R2*exp(phi0*lambda)*cos(4*pi1*x[0])*sin(2*pi1*x[1])+15*pi1*phi02*rzeta*exp(phi0*lambda)*cos(pi1*x[0])*cos(2*pi1*x[1])+20*pi1*phi02*exp(phi0*lambda)*cos(pi1*x[0])*cos(2*pi1*x[1])))/(2*phi02*(3*rzeta+4));

  }

private:
  const double R, rzeta, phi0, lambda, c1, c2;
};

//***************************************************************************
int main(int argc, char* argv[])
{
  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();
  parameters.add("n", 32, 1, std::numeric_limits<int>::max());

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Organise model parameters
  const dolfin::Constant phi0(parameters["phi0"]);
  const double rzeta = 5./3.;
  const double lambda = 27.0;
  const double R = 0.1;
  const dolfin::Constant phimin(parameters["phimin"]);
  const double phimax = 0.3;
  const double c1 = 0.5*(phimax+phimin);
  const double c2 = 0.5*(phimax-phimin);

  // Mesh
  const std::size_t nx = parameters["n"];
  dolfin::UnitSquareMesh mesh(nx, nx);

  // Function spaces
  Compaction2D::FunctionSpace W(mesh);
  dolfin::cout << "Number of degrees of freedom: " << W.dim() << dolfin::endl;
  dolfin::SubSpace W0(W, 0);

  // Set-up boundary conditions
  Boundary boundary;
  Inflow velocity_bc(R, rzeta, phi0, c1, c2);
  dolfin::DirichletBC bc(W0, velocity_bc, boundary);
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc);

  // Coefficient functions
  Source f(R, rzeta, phi0, lambda, c1, c2);
  eta myEta(lambda, phi0, c1, c2);
  kk  myK(R, rzeta, phi0, c1, c2);
  zetam1 myZetam1(phi0, rzeta, c1, c2);
  dolfin::Constant fp(0.0);

  // Create forms
  Compaction2D::BilinearForm a(W, W);
  Compaction2D::LinearForm L(W);
  L.f      = f;
  L.fp     = fp;
  a.eta    = myEta;
  a.kk     = myK;
  a.zetam1 = myZetam1;

  // Preconditioner form
  Compaction2D_PC::BilinearForm a_P(W, W);
  P22factor myP22fac(lambda, phi0, c1, c2);
  dolfin::Constant sgn(1.0);
  const std::string block_pc_type = parameters["block_pc_type"];
  if (block_pc_type == "triangular")
    sgn = -1.0;
  a_P.eta    = myEta;
  a_P.kk     = myK;
  a_P.zetam1 = myZetam1;
  a_P.p22fac = myP22fac;
  a_P.sgn    = sgn;

  // Solve problem
  dolfin::Function w(W);
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
	parameters["gmres_restart"], parameters["block_pc_type"],
	parameters["ksp_solver_type"], parameters["tol"], w);

  // Split solution
  dolfin::Function uu_deep = w[0];
  dolfin::Function pf_deep = w[1];
  dolfin::Function pc_deep = w[2];

  // Exact solution
  Exact_u ue(R, rzeta, phi0, c1, c2);
  Exact_v ve(R, rzeta, phi0, c1, c2);
  Exact_p pe;

  Error_sol::Functional error_u(mesh);
  error_u.u = uu_deep[0];
  error_u.exact = ue;
  double error_norm_u = sqrt(assemble(error_u));
  dolfin::info ("errors = {'u - u_e': %G," , error_norm_u) ;

  Error_sol::Functional error_v(mesh);
  error_v.u = uu_deep[1];
  error_v.exact = ve;
  double error_norm_v = sqrt(assemble(error_v));
  dolfin::info (" 'v - v_e': %G," , error_norm_v) ;

  PressureMean::Functional pmean(mesh);
  dolfin::Constant pm(0.0);
  pmean.p = pf_deep;
  double pressure_mean = assemble(pmean);

  *pf_deep.vector() -= pressure_mean;
  Error_sol::Functional error_p(mesh);
  error_p.u = pf_deep;
  error_p.exact = pe;
  double error_norm_p = sqrt(assemble(error_p));
  dolfin::info (" 'p - p_e': %G}" , error_norm_p) ;

  dolfin::info (" pressure mean old = %G" , pressure_mean);
  pmean.p = pf_deep;
  pressure_mean = assemble(pmean);
  dolfin::info (" pressure mean new = %G" , pressure_mean);
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
