# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(n, alpha, k_min, k_max, solver, num_smoother, pc_smoother, tol):
    """Run solver with given command line options and return output"""
    print "Command:", "./compaction-unit-square" + ' --n=' + str(n) \
                                                 + ' --alpha=' + str(alpha) \
                                                 + ' --k_min=' + str(k_min) \
                                                 + ' --k_max=' + str(k_max) \
                                                 + ' --solver=' + solver \
                                                 + ' --amg_num_smoother=' + str(num_smoother) \
                                                 + ' --amg_smoother_pc=' + pc_smoother \
                                                 + ' --tol=' + str(tol)
    return subprocess.check_output(['./compaction-unit-square', \
                                    '--n=' + str(n) , \
                                    '--alpha='  + str(alpha),\
                                    '--k_min=' + str(k_min), \
                                    '--k_max='  + str(k_max), \
                                    '--solver=' + solver, \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother, \
                                    '--tol=' + str(tol)])

def table_6_1():

    # Table data
    meshes = [32, 64, 128, 256]
    linearsolvers = ["minres"]
    solvers = ["lu", "amg"]
    alphas = [-1.0/3.0, 0, 1, 10, 100, 1000]

    f = open('table-6-1-data.txt', 'w')
    data = dict()
    for linsol in linearsolvers:
        data[linsol] = dict()
        if linsol == "minres":
            bpc_type = "diagonal"
        for mesh in meshes:
            data[linsol][mesh] = dict()
            for solver in solvers:
                data[linsol][mesh][solver] = dict()
                for alpha in alphas:
                    num_smoother = 4
                    pc_smoother = "sor"
                    if abs(alpha+1.0/3.0)<1.0e-10:
                        tol=1.0e-12
                    elif abs(alpha-1000)<1.0e-10:
                        tol=1.0e-12
                    else:
                        tol=1.0e-10
                    out = run_solver(n=mesh, alpha=alpha, \
                                         k_min=0.5, k_max=1.5, \
                                         solver=solver, \
                                         num_smoother=num_smoother, \
                                         pc_smoother=pc_smoother, \
                                         tol=tol)

                    # Parse output to get iteration count
                    data[linsol][mesh][solver][alpha] = get_iteration_count(out)

                    print out
                    f.write(out)

    # Pretty-print table
    for linsol in linearsolvers:
        table = linsol + "\n"
        table += "N".ljust(10)
        for alpha in alphas:
            for solver in solvers:
                table += ("(" + "{0:.2f}".format(alpha) + ", " + solver + ")").ljust(15)
        table += "\n"
        for mesh in meshes:
            table += str(mesh).ljust(10)
            for alpha in alphas:
                for solver in solvers:
                    table += str(data[linsol][mesh][solver][alpha]).ljust(15)
            table += "\n"

        print table
        f.write(table)

    # Close file
    del f


# Run Table 6.1
table_6_1()
