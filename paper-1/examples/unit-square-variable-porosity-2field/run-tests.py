# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(n, phimin, phi0, solver, num_smoother, pc_smoother, tol):
    """Run solver with given command line options and return output"""
    print "Command:", "./compaction-var-porosity" + ' --n=' + str(n) \
                                                 + ' --phimin=' + str(phimin) \
                                                 + ' --phi0=' + str(phi0) \
                                                 + ' --solver=' + solver \
                                                 + ' --amg_num_smoother=' + str(num_smoother) \
                                                 + ' --amg_smoother_pc=' + pc_smoother \
                                                 + ' --tol=' + str(tol)
    return subprocess.check_output(['./compaction-var-porosity', \
                                    '--n=' + str(n) , \
                                    '--phimin='  + str(phimin),\
                                    '--phi0=' + str(phi0), \
                                    '--solver=' + solver, \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother, \
                                    '--tol=' + str(tol)])


def table_6_4():

    # Table data
    meshes = [32, 64, 128, 256]
    solvers = ["lu", "amg"]
    phimins = [1.0e-3]

    f = open('table-6-4-data.txt', 'w')
    data = dict()
    for mesh in meshes:
        data[mesh] = dict()
        for solver in solvers:
            data[mesh][solver] = dict()
            for phimin in phimins:
                num_smoother = 4
                pc_smoother = "sor"
                out = run_solver(n=mesh, solver=solver, phimin=phimin, \
                                 phi0=0.05, \
                                 num_smoother=num_smoother, \
                                 pc_smoother=pc_smoother, \
                                 tol=1.0e-10)

                # Parse output to get iteration count
                data[mesh][solver][phimin] = get_iteration_count(out)

                print out
                f.write(out)

    # Pretty-print table
    table = "N".ljust(10)
    for phimin in phimins:
        for solver in solvers:
            table += ("(" + "{0:.2f}".format(phimin) + ", " + solver + ")").ljust(15)
    table += "\n"
    for mesh in meshes:
        table += str(mesh).ljust(10)
        for phimin in phimins:
            for solver in solvers:
                table += str(data[mesh][solver][phimin]).ljust(15)
        table += "\n"
    print table
    f.write(table)

    # Close file
    del f

# Run Table 6.4
table_6_4()
