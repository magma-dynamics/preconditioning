# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(meshfile, phi0, alpha, solver, num_smoother, pc_smoother, gmres_restart, tol, bpc_type, ksp_type):
    """Run solver with given command line options and return output"""

    print "Command:", "./compaction-3d-wedge" + ' --mesh_file=' + str(meshfile) \
                                              + ' --phi0=' + str(phi0) \
                                              + ' --alpha=' + str(alpha) \
                                              + ' --solver=' + solver \
                                              + ' --tol=' + str(tol) \
                                              + ' --amg_num_smoother=' + str(num_smoother) \
                                              + ' --amg_smoother_pc=' + pc_smoother \
                                              + ' --gmres_restart=' + str(gmres_restart) \
                                              + ' --block_pc_type=' + str(bpc_type) \
                                              + ' --ksp_solver_type=' + str(ksp_type)

#    return subprocess.check_output(['./compaction-3d-wedge', \
    return subprocess.check_output(['mpiexec', '-n', '8',
                                    './compaction-3d-wedge', \
                                    '--mesh_file=' + meshfile , \
                                    '--phi0='  + str(phi0),\
                                    '--alpha='  + str(alpha),\
                                    '--solver=' + solver, \
                                    '--tol=' + str(tol), \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother, \
                                    '--gmres_restart=' + str(gmres_restart), \
                                    '--block_pc_type=' + str(bpc_type), \
                                    '--ksp_solver_type=' + str(ksp_type)])

def table_6_7():

    # Table data
    linearsolvers = ["minres", "bicgstab", "gmres"]
    meshes = ["../wedge3D-meshes/wedge_3D_cells_79477.xml", \
              "../wedge3D-meshes/wedge_3D_cells_370841.xml", \
              "../wedge3D-meshes/wedge_3D_cells_1683231.xml"]

    f = open('table-6-7-data.txt', 'w')
    data = dict()
    for linsol in linearsolvers:
        data[linsol] = dict()
        if linsol == "minres":
            bpc_type = "diagonal"
        else:
            bpc_type = "triangular"
        for mesh in meshes:
            num_smoother = 4
            pc_smoother = "sor"
            out = run_solver(meshfile=mesh, solver="amg", \
                                 phi0=0.01, alpha=1000, \
                                 num_smoother=num_smoother, \
                                 pc_smoother=pc_smoother, \
                                 gmres_restart=100, \
                                 tol=1.0e-10, \
                                 bpc_type=bpc_type, \
                                 ksp_type=linsol)

            # Parse output to get iteration count
            data[linsol][mesh] = get_iteration_count(out)

            print out
            f.write(out)

    # Pretty-print table
    for linsol in linearsolvers:
        table = linsol + "\n"
        table += "N".ljust(14)
        table += "\n"
        for mesh in meshes:
            table += str(mesh)[-12:].ljust(14)
            table += str(data[linsol][mesh]).ljust(15)
            table += "\n"
        print table
        f.write(table)

    # Close file
    del f
    print "End of script"

# Run Table 6.7
table_6_7()
