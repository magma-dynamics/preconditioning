// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <limits>
#include <petsc.h>
#include <dolfin.h>

#include "../../common/create_field_split.h"
#include "../../common/parameters.h"
#include "../../common/linear_solver.h"
#include "../../common/set_nullspace.h"

#include "../../forms/Compaction2D.h"
#include "../../forms/Compaction2D_PC.h"
#include "../../forms/Error_sol.h"
#include "../../forms/PressureMean.h"

#if defined(HAS_PETSC)

//***************************************************************************
// Define boundary domain
class Boundary : public dolfin::SubDomain
{
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return on_boundary; }
};

//***************************************************************************
// Boundary solution
class Inflow : public dolfin::Expression
{
public:
  Inflow(double c1, double c2, double t1, double t2)
    : Expression(2), c1(c1), c2(c2), t1(t1), t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double kk   = c2*(tanh(t1*x[0] - t2) + tanh(t1*x[1] - t2) + 2.0 + c1);
    const double dpdx = 4*DOLFIN_PI*sin(4*DOLFIN_PI*x[0])*cos(2*DOLFIN_PI*x[1]);
    const double dpdy = 2*DOLFIN_PI*cos(4*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1]);
    const double w1   = sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]) + 2.0;
    const double w2   = 0.5*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdx + w1;
    values[1] = kk*dpdy + w2;
  }

private:
  const double c1, c2, t1, t2;
};

//***************************************************************************
// Exact solutions
class Exact_u : public dolfin::Expression
{
public:
  Exact_u(double c1, double c2, double t1, double t2) : c1(c1), c2(c2),
                                                        t1(t1), t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double kk = c2*( tanh(t1*x[0]-t2) + tanh(t1*x[1]-t2) + 2.0 + c1);
    const double dpdx = 4*DOLFIN_PI*sin(4*DOLFIN_PI*x[0])*cos(2*DOLFIN_PI*x[1]);
    const double w1 = sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdx + w1;
  }

private:
  const double c1, c2, t1, t2;
};

class Exact_v : public dolfin::Expression
{
public:
  Exact_v(double c1, double c2, double t1, double t2) : c1(c1), c2(c2),
                                                        t1(t1), t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double kk = c2*( tanh(t1*x[0]-t2) + tanh(t1*x[1]-t2) + 2.0 + c1);
    const double dpdy = 2*DOLFIN_PI*cos(4*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1]);
    const double w2 = 0.5*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]) + 2.0;
    values[0] = kk*dpdy + w2;
  }

private:
  const double c1, c2, t1, t2;
};

class Exact_p : public dolfin::Expression
{
public:
  //Exact_p(double c1, double c2) : c1(c1), c2(c2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = (-cos(4.0*DOLFIN_PI*x[0]))*cos(2.0*DOLFIN_PI*x[1]); }

//private:
//  const double c1, c2;
};

//***************************************************************************
// Permeability
class kk : public dolfin::Expression
{
public:
  kk(double c1, double c2, double t1, double t2) : c1(c1), c2(c2), t1(t1),
                                                   t2(t2) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = c2*(tanh(t1*x[0] - t2)+tanh(t1*x[1] - t2) +2.0 +c1); }

private:
  const double c1, c2, t1, t2;
};

class zetam1 : public dolfin::Expression
{
public:
  zetam1(double alpha) : alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = 1.0 / (alpha + 1./3. + 1.e-13); }

private:
  const double alpha;
};

class eta : public dolfin::Expression
{
public:
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = 1.0; }
};

class P22factor : public dolfin::Expression
{
public:
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    double eta0 = 1.0;
    values[0] = 1.0/eta0;
  }
};

//***************************************************************************
// Source term
class Source : public dolfin::Expression
{
public:
  Source(double c1, double c2, double t1, double t2, double alpha)
    : Expression(2), c1(c1), c2(c2), t1(t1), t2(t2), alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double pi3 = DOLFIN_PI*DOLFIN_PI*DOLFIN_PI;
    const double pi2 = DOLFIN_PI*DOLFIN_PI;
    values[0] = -0.5*(-16.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-8.0*DOLFIN_PI*c2*t1*t1*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))*tanh(t1*x[1]-t2)-16.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-4.0*pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]))+(-alpha-0.5)*(-16.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-8.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))+1.0*pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])+4.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*cos(2.0*DOLFIN_PI*x[1]))+(-alpha-1.0)*(-64.0*pi3*c2*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-pi2*sin(DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])-8.0*DOLFIN_PI*c2*t1*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*tanh(t1*x[0]-t2)*cos(2.0*DOLFIN_PI*x[1])+32.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*cos(2.0*DOLFIN_PI*x[1]))+4.0*DOLFIN_PI*sin(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]);

    values[1] = (-alpha-1.0)*(-8.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-4.0*DOLFIN_PI*c2*t1*t1*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))*tanh(t1*x[1]-t2)+8.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-2.0*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))+(-alpha-0.5)*(-32.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)+16.0*pi2*c2*t1*cos(4.0*DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1])*(1./cosh(t1*x[1]-t2))*(1./cosh(t1*x[1]-t2))-8.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*sin(2.0*DOLFIN_PI*x[1])+2.0*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))-0.5*(-32.0*pi3*c2*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1])*(tanh(t1*x[1]-t2)+tanh(t1*x[0]-t2)+c1+2.0)-4.0*DOLFIN_PI*c2*t1*t1*cos(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*tanh(t1*x[0]-t2)*sin(2.0*DOLFIN_PI*x[1])-16.0*pi2*c2*t1*sin(4.0*DOLFIN_PI*x[0])*(1./cosh(t1*x[0]-t2))*(1./cosh(t1*x[0]-t2))*sin(2.0*DOLFIN_PI*x[1])-0.5*pi2*cos(DOLFIN_PI*x[0])*cos(2.0*DOLFIN_PI*x[1]))+2.0*DOLFIN_PI*cos(4.0*DOLFIN_PI*x[0])*sin(2.0*DOLFIN_PI*x[1]);

  }

private:
  const double c1, c2, t1, t2, alpha;
};

//***************************************************************************
int main(int argc, char* argv[])
{
  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();
  parameters.add("n", 32, 1, std::numeric_limits<int>::max());

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Organise model parameters
  const double k_low = parameters["k_min"];
  const double k_up  = parameters["k_max"];
  const dolfin::Constant alpha(parameters["alpha"]);

  const double t1 = 10.0;
  const double t2 = 5.0;
  const double frakx = 2.0*tanh(t1 - t2) + 2.0;
  const double fraky = 2.0*tanh(-t2) + 2.0;
  const double c1 = (k_up*fraky - k_low*frakx)/(k_low-k_up);
  const double c2 = (k_up-k_low)/(frakx - fraky);

  // Mesh
  const std::size_t nx = parameters["n"];
  dolfin::UnitSquareMesh mesh(MPI_COMM_WORLD, nx, nx);

  // Function spaces
  Compaction2D::FunctionSpace W(mesh);
  dolfin::SubSpace W0(W, 0);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::cout << "Number of degrees of freedom: " << W.dim() << dolfin::endl;

  // Set-up boundary conditions
  Boundary boundary;
  Inflow velocity_bc(c1, c2, t1, t2);
  dolfin::DirichletBC bc(W0, velocity_bc, boundary);
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc);

  // Coefficient functions
  Source f(c1, c2, t1, t2, alpha);
  eta myEta;
  kk  myK(c1, c2, t1, t2);
  zetam1 myZetam1(alpha);
  dolfin::Constant fp(0.0);

  // Create forms
  Compaction2D::BilinearForm a(W, W);
  Compaction2D::LinearForm L(W);
  L.f      = f;
  L.fp     = fp;
  a.eta    = myEta;
  a.kk     = myK;
  a.zetam1 = myZetam1;

  // Preconditioner form
  Compaction2D_PC::BilinearForm a_P(W, W);
  P22factor myP22fac;
  dolfin::Constant sgn(1.0);
  const std::string block_pc_type = parameters["block_pc_type"];
  if (block_pc_type == "triangular")
    sgn = -1.0;

  a_P.eta    = myEta;
  a_P.kk     = myK;
  a_P.zetam1 = myZetam1;
  a_P.p22fac = myP22fac;
  a_P.sgn    = sgn;

  // Solve problem
  dolfin::Function w(W);
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
	parameters["gmres_restart"], parameters["block_pc_type"],
	parameters["ksp_solver_type"], parameters["tol"], w);

  // Split solution (deep copies)
  dolfin::Function uu_deep = w[0];
  dolfin::Function pf_deep = w[1];
  dolfin::Function pc_deep = w[2];

  // Exact solution
  Exact_u ue(c1, c2, t1, t2);
  Exact_v ve(c1, c2, t1, t2);
  Exact_p pe;

  Error_sol::Functional error_u(mesh);
  error_u.u = uu_deep[0];
  error_u.exact = ue;
  const double error_norm_u = sqrt(assemble(error_u));

  Error_sol::Functional error_v(mesh);
  error_v.u = uu_deep[1];
  error_v.exact = ve;
  const double error_norm_v = sqrt(assemble(error_v));

  PressureMean::Functional pmean(mesh);
  dolfin::Constant pm(0.0);
  pmean.p = pf_deep;
  const double pressure_mean_old = assemble(pmean);

  *pf_deep.vector() -= pressure_mean_old;
  Error_sol::Functional error_p(mesh);
  error_p.u = pf_deep;
  error_p.exact = pe;
  const double error_norm_p = sqrt(assemble(error_p));

  pmean.p = pf_deep;
  const double pressure_mean = assemble(pmean);

  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
  {
    dolfin::info ( "errors = {'u - u_e': %G," , error_norm_u);
    dolfin::info ( " 'v - v_e': %G," , error_norm_v);
    dolfin::info ( " 'p - p_e': %G}" , error_norm_p);
    dolfin::info ( " pressure mean old = %G" , pressure_mean_old);
    dolfin::info ( " pressure mean new = %G" , pressure_mean);
  }
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
