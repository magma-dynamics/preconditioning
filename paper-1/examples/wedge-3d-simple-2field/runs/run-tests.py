# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(meshfile, phi0, alpha, solver, num_smoother, pc_smoother, tol):
    """Run solver with given command line options and return output"""
    print "Command:", "./compaction-3d-wedge" + ' --mesh_file=' + meshfile \
                                                 + ' --alpha=' + str(alpha) \
                                                 + ' --phi0=' + str(phi0) \
                                                 + ' --solver=' + solver \
                                                 + ' --amg_num_smoother=' + str(num_smoother) \
                                                 + ' --amg_smoother_pc=' + pc_smoother \
                                                 + ' --tol=' + str(tol)

#    return subprocess.check_output(['mpiexec', '-n', '16',
#                                    './compaction-3d-wedge', \
    return subprocess.check_output(['./compaction-3d-wedge', \
                                    '--mesh_file=' + meshfile , \
                                    '--alpha='  + str(alpha),\
                                    '--phi0='  + str(phi0),\
                                    '--solver=' + solver, \
                                    '--tol=' + str(tol), \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother])

def table_6_7():

    # Table data
    meshes = ["../wedge3D-meshes/wedge_3D_cells_79477.xml"]
#    meshes = ["../wedge3D-meshes/wedge_3D_cells_79477.xml", \
#              "../wedge3D-meshes/wedge_3D_cells_370841.xml", \
#              "../wedge3D-meshes/wedge_3D_cells_1683231.xml"]

    alphas = [1000]

    f = open('table-6-7-data.txt', 'w')
    table = ""
    data = dict()
    for mesh in meshes:
        data[mesh] = dict()
        for alpha in alphas:
            num_smoother = 4
            pc_smoother = "sor"
            out = run_solver(meshfile=mesh, solver="amg", \
                             phi0=0.01, alpha=alpha,
                             num_smoother=num_smoother, \
                             pc_smoother=pc_smoother, \
                             tol=1.0e-10)

            # Parse output to get iteration count
            data[mesh][alpha] = get_iteration_count(out)

            print out
            f.write(out)
            f.write('\n\n')

    # Pretty-print table
    table += "N".ljust(14)
    for alpha in alphas:
        table += ("{0:.2f}".format(alpha)).ljust(15)
    table += "\n"
    for mesh in meshes:
        table += str(mesh)[-12:].ljust(14)
        for alpha in alphas:
            table += str(data[mesh][alpha]).ljust(15)
        table += "\n"

    print table
    f.write(table)

    # Close file
    del f
    print "End of script"

# Run Table 6.7
table_6_7()
