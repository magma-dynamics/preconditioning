# Simple 3D wedge test

To compile:

    cmake .
    make

Downloading meshes

Mesh file are on Bitbucket. To get the mesh XML files, from this directory:

1. wget https://bitbucket.org/magma-dynamics/preconditioning/downloads/wedge3D-meshes.tar.xz
2. tar xf wedge3D-meshes.tar.xz

To run:

   cp compaction-3d-wedge runs/

   cd runs/

   python run-tests.py