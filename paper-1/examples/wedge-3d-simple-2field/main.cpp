// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <petsc.h>
#include <dolfin.h>

#include "../../common/create_field_split_2field.h"
#include "../../common/parameters_2field.h"
#include "../../common/linear_solver_2field.h"
#include "../../common/set_nullspace.h"

#include "../../forms/Compaction3D_2field.h"
#include "../../forms/Compaction3D_PC_2field.h"

#if defined(HAS_PETSC)

//*************************************************************************
// Define boundaries
class Gamma0 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[0] + x[2] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class Gamma1 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[2] - 1.0) < DOLFIN_EPS && on_boundary); }
};

//*************************************************************************
// Permeability 
class kk : public dolfin::Expression
{
public:
  kk(double alpha) : alpha(alpha) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double rr = std::sqrt(x[0]*x[0] + x[2]*x[2]);
    values[0] = 0.9*(1.0 + tanh(-2.0*rr));
  }
private:
   const double alpha;
};

class zeta : public dolfin::Expression
{
public:
  zeta(double alpha) : alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = alpha + 1./3.; }
private:
  double alpha;
};

class eta : public dolfin::Expression
{
public:
  eta(double alpha) : alpha(alpha) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  { values[0] = 1.0; }
private:
  double alpha;
};

//***************************************************************************
// Source term
class Source : public dolfin::Expression
{
public:
  Source(double phi0)
    : Expression(3), phi0(phi0) {}
  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = phi0;
  }
private:
  double phi0;
};

//*************************************************************************
// Main program
int main(int argc, char* argv[])
{
  // Allow extrapolation
  dolfin::parameters["allow_extrapolation"] = true;

  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Organise model parameters
  const dolfin::Constant alpha(parameters["alpha"]);
  const dolfin::Constant phi0(parameters["phi0"]);

  // Mesh (see README.md on getting the meshes)
  const std::string mesh_file = parameters["mesh_file"];
  dolfin::Mesh mesh(mesh_file);

  // Create function (sub)spaces
  Compaction3D_2field::FunctionSpace W(mesh);
  dolfin::cout << "Number of dofs: " << W.dim() << dolfin::endl;
  dolfin::SubSpace W0(W, 0);
  dolfin::SubSpace W1(W, 1);
  dolfin::Function w(W);
  dolfin::Function uu = w[0];
  dolfin::Function pf = w[1];

  // Set-up boundary conditions
  Gamma0 g0;
  Gamma1 g1;
  const dolfin::Constant u_slope(1.0/sqrt(2.0), 0.1, -1.0/sqrt(2.0));
  const dolfin::Constant zero_vector(0.0, 0.0, 0.0);
  const dolfin::DirichletBC bc0(W0, u_slope    , g0);
  const dolfin::DirichletBC bc1(W0, zero_vector, g1);
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc0);
  bcs.push_back(&bc1);

  // Coefficient functions
  kk myK(alpha);
  zeta myZeta(alpha);
  eta myEta(alpha);
  Source mySource(phi0);
  dolfin::Constant f(0.0, 0.0, 1.0);
  dolfin::Constant fp(0.0);

  // Create forms
  Compaction3D_2field::BilinearForm a(W, W);
  Compaction3D_2field::LinearForm L(W);
  L.source = mySource;
  L.f      = f;
  L.fp     = fp;
  L.kk     = myK;
  a.eta    = myEta;
  a.kk     = myK;
  a.zeta   = myZeta;

  // Preconditioner form
  Compaction3D_PC_2field::BilinearForm a_P(W, W);
  dolfin::Constant sgn(1.0);
  if (std::string(parameters["split_type"]) == "schur")
    sgn = -1.0;
  a_P.eta    = myEta;
  a_P.kk     = myK;
  a_P.zeta   = myZeta;

  // Solve problem
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
        parameters["tol"], w);
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
