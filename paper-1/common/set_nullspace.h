// Copyright (C) 2014 Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BUILD_NULLSPACE_H
#define __BUILD_NULLSPACE_H

#include <memory>
#include <vector>
#include <dolfin/common/MPI.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/function/SubSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/VectorSpaceBasis.h>

// This function is used to build a null space basis for a 2D or 3D
// elasticity-type problem

dolfin::VectorSpaceBasis
build_nullspace(const dolfin::FunctionSpace& V,
                const std::vector<dolfin::la_index>& sub_indices,
                std::pair<std::size_t, std::size_t> range,
                std::vector<std::size_t>& local_to_global_map0)
{
  dolfin_assert(V.mesh());
  const dolfin::Mesh& mesh = *(V.mesh());
  const std::size_t gdim = mesh.geometry().dim();
  const std::size_t basis_dim = (gdim == 2 ? 3 : 6);

  // Ghost indices (empty)
  const std::vector<dolfin::la_index> ghost_indices;

  // Get subspaces
  std::vector<dolfin::SubSpace> sub_spaces;
  for (std::size_t i = 0; i < gdim; ++i)
    sub_spaces.push_back(dolfin::SubSpace(V, i));

  //  Create 'parent' vector to hold nullspace components
  dolfin::Vector x;
  x.init(MPI_COMM_WORLD, range, local_to_global_map0, ghost_indices);
  const std::size_t parent_offset = range.first;

  // Local range
  const std::size_t local_size = sub_indices.size();
  const std::size_t offset
    = dolfin::MPI::global_offset(MPI_COMM_WORLD, local_size, true);
  const std::pair<std::size_t, std::size_t>
    local_range(offset, offset + local_size);

  // Local-to-global map
  std::vector<std::size_t> local_to_global_map(local_size, 0);
  for (std::size_t i = 0; i < local_size; ++i)
    local_to_global_map[i] = i + offset;

  std::vector<std::shared_ptr<dolfin::GenericVector> > basis(basis_dim);
  for (std::size_t i = 0; i < basis.size(); ++i)
  {
    // Create vector
    basis[i]
      = std::shared_ptr<dolfin::GenericVector>(new dolfin::Vector());
    dolfin_assert(basis[i]);

    // Intialise and zero
    basis[i]->init(MPI_COMM_WORLD, local_range, local_to_global_map,
                   ghost_indices);
    basis[i]->zero();
  }

  std::vector<double> old_values, new_values(sub_indices.size());

  // x0, x1, (x2) translations
  for (std::size_t i = 0; i < gdim; ++i)
  {
    x.zero();
    sub_spaces[i].dofmap()->set(x, 1.0);
    x.apply("set");
    x.get_local(old_values);
    for (std::size_t j = 0; j < sub_indices.size(); ++j)
    {
      const std::size_t index = sub_indices[j] - parent_offset;
      new_values[j] = old_values[index];
    }
    basis[i]->set_local(new_values);
  }

  // Rotations
  x.zero();
  sub_spaces[0].dofmap()->set_x(x, -1.0, 1, mesh);
  sub_spaces[1].dofmap()->set_x(x,  1.0, 0, mesh);
  x.apply("set");
  x.get_local(old_values);
  for (std::size_t i = 0; i < sub_indices.size(); ++i)
  {
    const std::size_t index = sub_indices[i] - parent_offset;
    new_values[i] = old_values[index];
  }
  basis[gdim]->set_local(new_values);

  if (gdim == 3)
  {
    x.zero();
    sub_spaces[0].dofmap()->set_x(x,  1.0, 2, mesh);
    sub_spaces[2].dofmap()->set_x(x, -1.0, 0, mesh);
    x.apply("set");
    x.get_local(old_values);
    for (std::size_t i = 0; i < sub_indices.size(); ++i)
    {
      const std::size_t index = sub_indices[i] - parent_offset;
      new_values[i] = old_values[index];
    }
    basis[4]->set_local(new_values);

    x.zero();
    sub_spaces[2].dofmap()->set_x(x,  1.0, 1, mesh);
    sub_spaces[1].dofmap()->set_x(x, -1.0, 2, mesh);
    x.apply("set");
    x.get_local(old_values);
    for (std::size_t i = 0; i < sub_indices.size(); ++i)
    {
      const std::size_t index = sub_indices[i] - parent_offset;
      new_values[i] = old_values[index];
    }
    basis[5]->set_local(new_values);
  }

  // Apply
  for (std::size_t i = 0; i < basis.size(); ++i)
    basis[i]->apply("add");

  return dolfin::VectorSpaceBasis(basis);
}

#endif
