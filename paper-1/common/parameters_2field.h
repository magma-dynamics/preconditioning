// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAGMA_PARAMETERS_H
#define __MAGMA_PARAMETERS_H

#include <limits>
#include <set>
#include <string>
#include <dolfin/parameter/Parameters.h>

// This function returns default model parameters

dolfin::Parameters default_parameters()
{
  // Create parameters object
  dolfin::Parameters parameters("magma");

  // Equation parameters
  parameters.add("alpha" , 0.0,   -1.0/3.0, 1.0e6);
  parameters.add("k_min" , 0.0,    0.0, std::numeric_limits<double>::max());
  parameters.add("k_max" , 1.0e-4, 0.0, std::numeric_limits<double>::max());
  parameters.add("phimin", 0.01,   0.0, 1.0);
  parameters.add("phi0"  , 0.01,   0.0, 1.0);

  // Solver type (AMG or LU)
  std::set<std::string> allowable_solvers;
  allowable_solvers.insert("amg");
  allowable_solvers.insert("lu");
  parameters.add("solver", "amg", allowable_solvers);

  // AMG smoother applications
  parameters.add("amg_num_smoother", 2, 1, 100);

  // AMG smoother preconditioner type
  std::set<std::string> allowable_amg_smoother_pc;
  allowable_amg_smoother_pc.insert("jacobi");
  allowable_amg_smoother_pc.insert("sor");
  parameters.add("amg_smoother_pc", "jacobi", allowable_amg_smoother_pc);

  // Solver convergence tolerance
  parameters.add("tol", 1.0e-8);

  // Split type
  std::set<std::string> allowable_splittype;
  allowable_solvers.insert("schur");
  allowable_solvers.insert("additive");
  parameters.add("split_type", "additive", allowable_splittype);

  // Mesh file name
  parameters.add<std::string>("mesh_file");

  return parameters;
}

#endif
