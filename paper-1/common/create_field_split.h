// Copyright (C) 2014 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAGMA_CREATE_FIELDSPLITS_H
#define __MAGMA_CREATE_FIELDSPLITS_H

#include <vector>
#include <petsc.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/function/SubSpace.h>
#include <dolfin/la/PETScVector.h>
#include <dolfin/la/VectorSpaceBasis.h>

#include "set_nullspace.h"

void create_fieldsplits(IS* is, const dolfin::FunctionSpace& W)
{
  // Get u, pf, pc subspaces
  dolfin::SubSpace W0(W, 0);
  dolfin::SubSpace W1(W, 1);
  dolfin::SubSpace W2(W, 2);

  MatNullSpace petsc_nullspace;

  // Create index sets for block structure
  const std::vector<dolfin::la_index> u_dofs = W0.dofmap()->dofs();
  const std::vector<dolfin::la_index> pf_dofs = W1.dofmap()->dofs();
  const std::vector<dolfin::la_index> pc_dofs = W2.dofmap()->dofs();
  dolfin::cout << "Number of u, pf and pc dofs on rank "
               << dolfin::MPI::rank(W.mesh()->mpi_comm()) << ": "
               << u_dofs.size() << ", " << pf_dofs.size() << ", "
               << pc_dofs.size() << dolfin::endl;

  ISCreateGeneral(PETSC_COMM_WORLD, u_dofs.size(), u_dofs.data(),
                  PETSC_COPY_VALUES, &is[0]);
  ISCreateGeneral(PETSC_COMM_WORLD, pf_dofs.size(), pf_dofs.data(),
                  PETSC_COPY_VALUES, &is[1]);
  ISCreateGeneral(PETSC_COMM_WORLD, pc_dofs.size(), pc_dofs.data(),
                  PETSC_COPY_VALUES, &is[2]);

  // Build nullspace for velocity block
  const std::pair<std::size_t, std::size_t> range
    = W.dofmap()->ownership_range();

  std::vector<std::size_t> local_to_global_map;
  W.dofmap()->tabulate_local_to_global_dofs(local_to_global_map);

  const dolfin::VectorSpaceBasis nullspace
    = build_nullspace(W0, u_dofs, range,
                      local_to_global_map);

  // Copy vectors
  std::vector<dolfin::PETScVector> petsc_nullspace_vectors;
  for (std::size_t i = 0; i < nullspace.dim(); ++i)
  {
    dolfin_assert(nullspace[i]);
    const dolfin::PETScVector&
      x = nullspace[i]->down_cast<dolfin::PETScVector>();

    // Copy vector
    petsc_nullspace_vectors.push_back(x);
  }

  // Get pointers to underlying PETSc objects
  std::vector<Vec> petsc_vec(nullspace.dim());
  for (std::size_t i = 0; i < nullspace.dim(); ++i)
    petsc_vec[i] = petsc_nullspace_vectors[i].vec();

  // Create PETSc null space
  PetscErrorCode ierr;
  ierr = MatNullSpaceCreate(PETSC_COMM_WORLD, PETSC_FALSE, nullspace.dim(),
                            petsc_vec.data(), &petsc_nullspace);
  if (ierr != 0)
    dolfin::error("Something went wrong building PETSc nullspace");

  // Attach (near) null space to velocity index set
  PetscObjectCompose((PetscObject) is[0], "nearnullspace",
                     (PetscObject) petsc_nullspace);
}

#endif
