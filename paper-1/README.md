This code is in support of the three-field preconditioning paper. The
results in the paper were produced with the below library versions:

FEniCS Project libraries (<http://fenicsproject.org>):

* DOLFIN - git revision d9ebb1673a78b1479722c8164c834725f632cfd6
* FFC - git revision 027eda7b74ebb7c7315801eae6fcca31fd823831
* FIAT - git revision e758fce7abbb64b6022a4f020ca8f5f9d66bb25c
* INSTANT - git revision 6618f89725a0abef469aabc3e56ac7cbd407cb85
* MSHR - git revision 8626388a043e656d92d0e18665f989abc0ed6306
* UFL - git revision 448ca2c4e111b280c72a531468dca8e9310a8a3f
* UFLACS - git revision 156ce9e655f9c7290ccaa52da75d8ae6e68ac1d4

* PETSc - version 3.5.2

Before building the example programs in `examples/`, see the README file
in `forms/`.