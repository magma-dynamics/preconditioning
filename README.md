# Preconditioners for magma dynamics

This repository contains source code for preconditioning strategies
for magma dynamics problems. It contains

- Code in `paper-0/` is supporting material for the paper

    Rhebergen, S., Wells, G.N., Katz, R.F. and Wathen,
    A.J. (2014). Analysis of block preconditioners for models of coupled
    magma/mantle dynamics. *SIAM Journal on Scientific Computing*
    36(4):A1960–1977. <http://dx.doi.org/10.1137/130946678>,
    <http://arxiv.org/abs/1311.6372>

    on two-field block preconditioners.

- Code in `paper-1/` is supporting material for the paper

    Rhebergen, S., Wells, G.N., Wathen, A.J. and Katz, R.F. Optimal
    three-field block-preconditioners for models of coupled magma/mantle
    dynamics. <http://arxiv.org/abs/1411.5235>

    on three-field preconditioners.


## License

This code  is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this code. If not, see <http://www.gnu.org/licenses/>.
