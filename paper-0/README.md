# Preconditioner for magma dynamics

This code is in support of the paper
<http://arxiv.org/abs/1311.6372>.


## Library versions

The results in the paper were produced with the below library
versions:

FEniCS Project libraries (<http://fenicsproject.org>):

* DOLFIN - git revision 3e17b79e49abd084bd476adcfaf789696de957e2
* FFC - git revision ba771bdf1a6474a92b506edf2f8a6698e700bd9e
* UFL - git revision 5aee174639eb48db330eea3195973be01778d056

* PETSc - git revision 06034a05e138d75f2716f20d68baf20bf4d61dc9
* ML - Trilinos version 10.6.1

Before building the example programs in `examples/`, see the README file
in `forms/`.


## Scripts to reproduce results

Each subdirectory of `exmaples/` contains a Python script
`run-tests.py` that can run the simulations used to produce the
results in the paper.

The outut logs for the results in <http://arxiv.org/abs/1311.6372> are
in the relevant subdirectories and are titled table-6-x-data-ref.txt.


## Note on convergence monitoring

PETSc tests for convergence in the preconditioned residual, whereas
the paper reports results for the true (unpreconditioend)
residual. Simulations are run to a preconditioned residual tolerance
at which a residual tolerance of 10^-8 in the unpreconditioned norm is
satisfied. The total number of iterations peformed in each example is
therefore greater than is required to satisfy a tolerance of 10^-8.