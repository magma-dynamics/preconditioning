// Copyright (C) 2013 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <petsc.h>
#include <dolfin.h>

#include "../../common/parameters.h"
#include "../../common/linear_solver.h"

#include "../../forms/Compaction3D.h"
#include "../../forms/Compaction3D_PC.h"

#if defined(HAS_PETSC)

// Define boundaries
class Gamma0 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[0] + x[2] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class Gamma1 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  { return (std::abs(x[2] - 1.0) < DOLFIN_EPS && on_boundary); }
};

class Gamma2 : public dolfin::SubDomain
{
public:
  bool inside(const dolfin::Array<double>& x, bool on_boundary) const
  {
    return ((std::abs(x[2]) < DOLFIN_EPS ||
               std::abs(x[0]-1.0) < DOLFIN_EPS) && on_boundary);
  }
};

// Permeability function k(x)
class Permeability : public dolfin::Expression
{
public:
  Permeability(double alpha) : alpha(alpha) {}

  void eval(dolfin::Array<double>& values, const dolfin::Array<double>& x) const
  {
    const double rr = std::sqrt(x[0]*x[0] + x[2]*x[2]);
    values[0] = 0.9*(1.0 + tanh(-2.0*rr));
  }

private:
   const double alpha;
};


// Main program
int main(int argc, char* argv[])
{
  // Get default model parameters and add problem-specific parameters
  dolfin::Parameters parameters = default_parameters();

  // Parse command line parameters and print parameters
  parameters.parse(argc, argv);
  if (dolfin::MPI::rank(MPI_COMM_WORLD) == 0)
    dolfin::info(parameters, true);

  // Mesh (see README.md on getting the meshes)
  const std::string mesh_file = parameters["mesh_file"];
  dolfin::Mesh mesh(mesh_file);

  // Constants
  const dolfin::Constant alpha(parameters["alpha"]);
  const dolfin::Constant phi0(parameters["phi0"]);

  // Create function (sub)spaces
  Compaction3D::FunctionSpace W(mesh);
  dolfin::cout << "Number of dofs: " << W.dim() << dolfin::endl;
  dolfin::SubSpace W0(W, 0);
  dolfin::SubSpace W1(W, 1);

  // Set-up boundary conditions
  const dolfin::Constant u_slope(1.0/sqrt(2.0), 0.1, -1.0/sqrt(2.0));
  const dolfin::Constant zero_vector(0.0, 0.0, 0.0);
  Gamma0 gamma0;
  Gamma1 gamma1;
  const dolfin::DirichletBC bc0(W0, u_slope, gamma0);
  const dolfin::DirichletBC bc1(W0, zero_vector, gamma1);

  // Collect boundary conditions
  std::vector<const dolfin::DirichletBC*> bcs;
  bcs.push_back(&bc0);
  bcs.push_back(&bc1);

  // Create forms for the "Stokes" part of the McKenzie equations
  Permeability k(alpha);
  const dolfin::Constant f(0.0, 0.0, 1.0);
  const dolfin::Constant fp(0.0);

  Compaction3D::BilinearForm a(W, W);
  Compaction3D::LinearForm L(W);
  L.f   = f;
  L.fp  = fp;
  L.phi = phi0;
  L.k   = k;
  a.alpha = alpha;
  a.k = k;

  // Preconditioner form
  Compaction3D_PC::BilinearForm a_P(W, W);

  // Preconditioner sign. PETSc switches sign of Schur-complement when
  // using "schur" fieldsplit, so the sign in the preconditioner needs
  // to be changed accordingly.
  dolfin::Constant sgn(1.0);
  if (std::string(parameters["split_type"]) == "schur")
    sgn = -1.0;

  // Attach coefficient functions to forms
  a_P.alpha = alpha;
  a_P.k     = k;
  a_P.sgn   = sgn;

  // Solve problem
  dolfin::Function w(W);
  solve(a_P, a, L, bcs, parameters["split_type"], parameters["solver"],
        parameters["amg_smoother_pc"], parameters["amg_num_smoother"],
        parameters["tol"], w);
}

#else

int main()
{
  dolfin::info("DOLFIN has not been configured with PETSc. Exiting.");
  return 0;
}

#endif
