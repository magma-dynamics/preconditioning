
// a characteristic length (lc) that sets the target
// element size at the point:

//N=4;  //mesh 0
N=8;  //mesh 1
//N=16; //mesh 2
//N=32; //mesh 3
//N=64; //mesh 4
//N=128; //mesh 5

lc=1./N;

Point(1) = {1,   0, 0, lc};
Point(2) = {1.5, 0, 0, lc};
Point(3) = {1.5, 1, 0, lc};
Point(4) = {1,   1, 0, lc};

Point(5) = {0,   0, 1, lc/8.};
Point(6) = {1.5, 0, 1, lc};
Point(7) = {1.5, 1, 1, lc};
Point(8) = {0,   1, 1, lc/8.};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};

Line(9)  = {2, 6};
Line(10) = {3, 7};
Line(11) = {4, 8};
Line(12) = {1, 5};

Line Loop(13) = {1,2,3,4};      Plane Surface(14) = {13};
Line Loop(15) = {5,6,7,8};      Plane Surface(16) = {15};
Line Loop(17) = {1,9,-5,-12};   Plane Surface(18) = {17};
Line Loop(19) = {-3,10,7,-11};  Plane Surface(20) = {19};
Line Loop(21) = {2,10,-6,-9};   Plane Surface(22) = {21};
Line Loop(23) = {4,12,-8,-11};  Plane Surface(24) = {23};

Surface Loop(35) = {14,16,18,20,22,24};

Volume(46) = {35};

Physical Volume(57) = {46};
