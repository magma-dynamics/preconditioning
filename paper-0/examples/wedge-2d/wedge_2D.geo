
// a characteristic length (lc) that sets the target
// element size at the point:
N=32;     //32, 64, 128
lc=1./N;
lcc=lc/8.;

Point(1) = {1, 0, 0, lc};
Point(2) = {1.5, 0, 0, lc};
Point(3) = {1.5, 1, 0, lc};
Point(4) = {0, 1, 0, lcc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(11) = {1,2,3,4};

Plane Surface(14) = {11};

Physical Line(1) = {1};
Physical Line(2) = {2,3};
Physical Line(3) = {4};

Physical Surface(1) = {14};
