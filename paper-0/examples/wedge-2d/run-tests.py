# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]

def run_solver(meshfile, alpha, batchelor, solver,
               num_smoother, pc_smoother, tol):
    """Run solver with given command line options and return output"""
    return subprocess.check_output(['./compaction-2d-wedge', \
                                    '--mesh_file=' + meshfile , \
                                    '--alpha='  + str(alpha),\
                                    '--batchelor_flow=' + str(batchelor), \
                                    '--solver=' + solver, \
                                    '--tol=' + str(tol), \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother])

def table_6_4_5():

    # Table data
    meshes = ["wedge2D-meshes/wedge_2D_cells_7435.xml", \
               "wedge2D-meshes/wedge_2D_cells_29427.xml", \
               "wedge2D-meshes/wedge_2D_cells_116448.xml"]

    solvers = ["lu", "amg"]
    alphas = [1, 10, 100, 1000]

    for batchelor in [True, False]:
        table = ""
        data = dict()
        if batchelor:
            f = open('table-6-4-data.txt', 'w')
        else:
            f = open('table-6-5-data.txt', 'w')
        for mesh in meshes:
            data[mesh] = dict()
            for alpha in alphas:
                data[mesh][alpha] = dict()
                if alpha == 1000:
                    num_smoother = 4
                    pc_smoother = "sor"
                else:
                    num_smoother = 2
                    pc_smoother = "jacobi"
                for solver in solvers:
                    out = run_solver(meshfile=mesh, solver=solver, \
                                     alpha=alpha, batchelor=batchelor, \
                                     num_smoother=num_smoother, \
                                     pc_smoother=pc_smoother, \
                                     tol=1.0e-8)

                    # Parse output to get iteration count
                    data[mesh][alpha][solver] = get_iteration_count(out)

                    print out
                    f.write(out)
                    f.write('\n\n')

        # Pretty-print table
        table += "N".ljust(14)
        for alpha in alphas:
            for solver in solvers:
                table += ("(" + "{0:.2f}".format(alpha) + ", " + solver + ")").ljust(15)
        table += "\n"
        for mesh in meshes:
            table += str(mesh)[-12:].ljust(14)
            for alpha in alphas:
                for solver in solvers:
                    table += str(data[mesh][alpha][solver]).ljust(15)
            table += "\n"

        table += "\n"

        print table
        f.write(table)

        # Close file
        del f

# Run Table 6.4 and 6.5
table_6_4_5()
