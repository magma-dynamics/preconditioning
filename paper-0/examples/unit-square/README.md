# Unit square test

To compile:

    cmake .
    make

To run:

    ./compaction-unit-square --n foo --alpha 1 --k_min 0.5 --k_max 1.5 --solver amg

where `foo` is the number of mesh vertices in each direction.

## Reproducing the results in Tables 6.1 - 6.3

Run the script

    python run-tests.py