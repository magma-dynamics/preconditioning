# Python script to run examples in paper

import subprocess

def get_iteration_count(out):
    """Parse output to get iteration count"""
    for line in out.splitlines():
        if "KSP" in line:
            line = line.split()
            if float(line[-1]) < 1.0e-8:
                return line[0]


def run_solver(n, alpha, k_min, k_max, solver, num_smoother, pc_smoother,
               tol):
    """Run solver with given command line options and return output"""
    print "Command:", "./compaction-unit-square" + ' --n=' + str(n) \
                                                 + ' --alpha=' + str(alpha) \
                                                 + ' --k_min=' + str(k_min) \
                                                 + ' --k_max=' + str(k_max) \
                                                 + ' --solver=' + solver \
                                                 + ' --amg_num_smoother=' + str(num_smoother) \
                                                 + ' --amg_smoother_pc=' + pc_smoother \
                                                 + ' --tol=' + str(tol)
    return subprocess.check_output(['./compaction-unit-square', \
                                    '--n=' + str(n) , \
                                    '--alpha='  + str(alpha),\
                                    '--k_min=' + str(k_min), \
                                    '--k_max='  + str(k_max), \
                                    '--solver=' + solver, \
                                    '--amg_num_smoother=' + str(num_smoother), \
                                    '--amg_smoother_pc=' + pc_smoother, \
                                    '--tol=' + str(tol)])


def table_6_1():

    # Table data
    meshes = [32, 64, 128, 256]
    solvers = ["lu", "amg"]
    alphas = [-1.0/3.0, 0, 1, 10, 100, 1000]

    f = open('table-6-1-data.txt', 'w')
    data = dict()
    for mesh in meshes:
        data[mesh] = dict()
        for solver in solvers:
            data[mesh][solver] = dict()
            for alpha in alphas:
                if alpha == 1000:
                    num_smoother = 4
                    pc_smoother = "sor"
                else:
                    num_smoother = 2
                    pc_smoother = "jacobi"
                out = run_solver(n=mesh, solver=solver, alpha=alpha, \
                                 k_min=0.5, k_max=1.5, \
                                 num_smoother=num_smoother, \
                                 pc_smoother=pc_smoother, \
                                 tol=1.0e-10)

                # Parse output to get iteration count
                data[mesh][solver][alpha] = get_iteration_count(out)

                print out
                f.write(out)

    # Pretty-print table
    table = "N".ljust(10)
    for alpha in alphas:
        for solver in solvers:
            table += ("(" + "{0:.2f}".format(alpha) + ", " + solver + ")").ljust(15)
    table += "\n"
    for mesh in meshes:
        table += str(mesh).ljust(10)
        for alpha in alphas:
            for solver in solvers:
                table += str(data[mesh][solver][alpha]).ljust(15)
        table += "\n"
    print table
    f.write(table)

    # Close file
    del f


def table_6_2_3():

    meshes  = [32, 64, 128, 256]
    solvers = ["lu", "amg"]
    alphas  = [1, 100]

    # k_max, k_min data
    k0 = [(1.0e-4, 0.0), (1.0e-4, 1.0e-8), (1.0e-4, 1.0e-6)]
    k1 = [(1.0, 0.0),    (1.0, 0.1),       (1.0, 0.5)]
    k2 = [(1.0e3, 0.0),  (1.0e3, 10.0),    (1.0e3, 100.0)]
    k_minmax = [k0, k1, k2]
    #k0 = [(1.0e-4, 0.0), (1.0e-4, 1.0e-8), (1.0e-4, 1.0e-6), (1.0e-4, 5.0e-5)]
    #k1 = [(1.0, 0.0),    (1.0, 0.1),       (1.0, 0.5),       (1.0, 0.9)]
    #k2 = [(1.0e3, 0.0),  (1.0e3, 1.0),     (1.0e3, 10.0),    (1.0e3, 100.0)]
    #k3 = [(1.0e8, 0.0),  (1.0e8, 1.0),     (1.0e8, 1000.0),  (1.0e8, 1.0e6)]
    #k_minmax = [k0, k1, k2, k3]

    for alpha in alphas:
        data = dict()
        if alpha == 1:
            f = open('table-6-2-data.txt', 'w')
        else:
            f = open('table-6-3-data.txt', 'w')
        for mesh in meshes:
            data[mesh] = dict()
            for solver in solvers:
                data[mesh][solver] = dict()
                for k0 in k_minmax:
                    for k in k0:
                        out = run_solver(n=mesh, solver=solver, alpha=alpha, \
                                         k_min=k[1], k_max=k[0],
                                         num_smoother=2, \
                                         pc_smoother="jacobi", \
                                         tol=1.0e-10)

                        # Parse output to get iteration count
                        data[mesh][solver][k] = get_iteration_count(out)

                        print out
                        f.write(out)
                        f.write('\n\n')


        # Pretty-print table
        table = "alpha = " + str(alpha) + "\n"
        for k0 in k_minmax:
            table += "K^* = " + "{0:g}".format(k0[0][0]) + "\n"
            table += "N".ljust(10)
            for k in k0:
                for solver in solvers:
                    table += ("(" + "{0:g}".format(k[1]) + ", " + solver + ")").ljust(15)
            table += "\n"
            for mesh in meshes:
                table += str(mesh).ljust(10)
                for k in k0:
                    for solver in solvers:
                        table += str(data[mesh][solver][k]).ljust(15)
                table += "\n"
            table += "\n"
        table += "\n"

        print table
        f.write(table)

        # Close file
        del f


# Run Table 6.1
table_6_1()

# Run Table 6.2 and 6.2
table_6_2_3()
