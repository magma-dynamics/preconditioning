# Sander Rhebergen
# University of Oxford
# Last update: 12 April 2013
#
# Solving Stokes with compaction.
#
#**********************************************************************************************


from dolfin import *
import numpy
from math import tanh

def compute(nx,kup,degree):

    if (abs(kup-10.0)<1.e-10):
        if (degree==21):
            if (nx==16):
                errors = {'u - u_e': 0.148723,
                          'v - v_e': 0.131818,
                          'p - p_e': 0.0483695}
            elif (nx==32):
                errors = {'u - u_e': 0.0114326,
                          'v - v_e': 0.010037,
                          'p - p_e': 0.0126243}
            elif (nx==64):
                errors = {'u - u_e': 0.000758076,
                          'v - v_e': 0.000687087,
                          'p - p_e': 0.00319109}
            elif (nx==128):
                errors = {'u - u_e': 4.56033E-05,
                          'v - v_e': 4.67051E-05,
                          'p - p_e': 0.000800024}
            elif (nx==256):
                errors = {'u - u_e': 4.32619E-06,
                          'v - v_e': 4.58417E-06,
                          'p - p_e': 0.00020013}

    if (abs(kup-1000.0)<1.e-12):
        if (degree==21):
            if (nx==16):
                errors = {'u - u_e': 14.8902,
                          'v - v_e': 13.1457,
                          'p - p_e': 0.0509992}            
            elif (nx==32):
                errors = {'u - u_e': 1.15879,
                          'v - v_e': 0.99608,
                          'p - p_e': 0.0134507}
            elif (nx==64):
                errors = {'u - u_e': 0.0784243,
                          'v - v_e': 0.0674423,
                          'p - p_e': 0.00341469}
            elif (nx==128):
                errors = {'u - u_e': 0.00500986,
                          'v - v_e': 0.00433851,
                          'p - p_e': 0.000857163}
            elif (nx==256):
                errors = {'u - u_e': 0.000402657,
                          'v - v_e': 0.000330436,
                          'p - p_e': 0.000213716}

    return errors

# Run for different meshes
degree=21
kup=1000.0

h = []  # element sizes
E = []  # errors
for nx in [16, 32, 64, 128, 256]:
    h.append(1.0/nx)
    E.append(compute(nx, kup, degree))  # list of dicts

# Convergence rates
from math import log as ln  # log is a dolfin name too
error_types = E[0].keys()
for error_type in sorted(error_types):
    print '\nError norm based on', error_type
    for i in range(1, len(E)):
        Ei   = E[i][error_type]  # E is a list of dicts
        Eim1 = E[i-1][error_type]
        r = ln(Ei/Eim1)/ln(h[i]/h[i-1])
        print 'h=%8.2E E=%8.2E r=%.2f' % (h[i], Ei, r)
    for i in range(1, len(E)):
        Ei   = E[i][error_type]  # E is a list of dicts
        Eim1 = E[i-1][error_type]
        r = ln(Ei/Eim1)/ln(h[i]/h[i-1])
        print '%8.2E & %.1f &' % (Ei, r)

