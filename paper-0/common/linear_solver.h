// Copyright (C) 2013 Sander Rhebergen and Garth N. Wells
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAGMA_LINRA_SOLVER_H
#define __MAGMA_LINRA_SOLVER_H

#include <string>
#include <petsc.h>
#include <dolfin/function/Function.h>
#include <dolfin/la/PETScMatrix.h>
#include <dolfin/la/PETScVector.h>

#include "create_field_split.h"

void solve(const dolfin::Form& a_P,
           const dolfin::Form& a,
           const dolfin::Form& L,
           const std::vector<const dolfin::DirichletBC*> bcs,
           std::string fieldsplit_type,
           std::string solver_type,
           std::string smoother_pc_type,
           std::size_t smoother_num_it,
           double convergence_tol,
           dolfin::Function& w)
{
  // Assembler preconditioner operator P
  std::shared_ptr<dolfin::PETScMatrix> P(new dolfin::PETScMatrix);
  dolfin::SystemAssembler assembler_pc(a_P, L, bcs);
  assembler_pc.assemble(*P);

  // Assemble system
  std::shared_ptr<dolfin::PETScMatrix> A(new dolfin::PETScMatrix);
  dolfin::PETScVector b;
  dolfin::SystemAssembler assembler(a, L, bcs);
  assembler.assemble(*A, b);

  // Get Function space
  dolfin_assert(w.function_space());
  const dolfin::FunctionSpace& W = *(w.function_space());

  // Build field splits and attach nullspace
  IS is[2];
  create_fieldsplits(is, W);

  // Create Krylov sover and set some parameters
  dolfin::PETScKrylovSolver solver("minres", "default");
  solver.parameters["relative_tolerance"] = convergence_tol;
  solver.parameters["error_on_nonconvergence"] = false;

  std::cout << "Set operators" << std::endl;
  std::shared_ptr<const dolfin::PETScBaseMatrix> _P = P;
  std::shared_ptr<const dolfin::PETScBaseMatrix> _A = A;
  solver.set_operators(_A, _P);

  // Unwrap PETSc solver and preconditioner
  KSP ksp = solver.ksp();

  // Get PETSc preconditioner
  PC pc;
  KSPGetPC(ksp, &pc);

  // Set preconditioner type
  PCSetType(pc, PCFIELDSPLIT);

  // Set field splits
  PCFieldSplitSetIS(pc, "u", is[0]);
  PCFieldSplitSetIS(pc, "p", is[1]);

  // Set some Krylov solver options
  dolfin::PETScOptions::set("ksp_monitor_true_residual");
  dolfin::PETScOptions::set("ksp_max_it", 2400);

  // Set field split type
  if (fieldsplit_type == "schur")
  {
    dolfin::PETScOptions::set("pc_fieldsplit_type", "schur");
    dolfin::PETScOptions::set("pc_fieldsplit_schur_fact_type", "diag");
    dolfin::PETScOptions::set("pc_fieldsplit_schur_precondition", "user");
  }
  else if (fieldsplit_type == "additive")
    dolfin::PETScOptions::set("pc_fieldsplit_type", "additive");

  if (solver_type == "amg")
  {
    // Multigrid for 00 block
    dolfin::PETScOptions::set("fieldsplit_u_ksp_type", "richardson");
    dolfin::PETScOptions::set("fieldsplit_u_pc_type", "ml"); // "gamg" or "ml"
    dolfin::PETScOptions::set("fieldsplit_u_ksp_max_it", 1);
    dolfin::PETScOptions::set("fieldsplit_u_pc_view");

    // AMG smoother options for 00 block
    dolfin::PETScOptions::set("fieldsplit_u_mg_levels_ksp_type", "chebyshev");
    dolfin::PETScOptions::set("fieldsplit_u_mg_levels_ksp_max_it", smoother_num_it);
    dolfin::PETScOptions::set("fieldsplit_u_mg_levels_pc_type", smoother_pc_type);
    if (smoother_pc_type == "sor")
      dolfin::PETScOptions::set("fieldsplit_u_mg_levels_pc_sor_its", 1);

    // Multiltigrid for 11 block
    dolfin::PETScOptions::set("fieldsplit_p_ksp_type", "preonly");
    dolfin::PETScOptions::set("fieldsplit_p_pc_type", "hypre");
  }
  else if (solver_type == "lu")
  {
    // LU for 00 block
    dolfin::PETScOptions::set("fieldsplit_u_ksp_type", "preonly");
    dolfin::PETScOptions::set("fieldsplit_u_pc_type", "lu");

    // LU for 11 block
    dolfin::PETScOptions::set("fieldsplit_p_ksp_type", "preonly");
    dolfin::PETScOptions::set("fieldsplit_p_pc_type", "lu");
  }

  KSPSetFromOptions(ksp);

  std::cout << "Call solve" << std::endl;
  const std::size_t num_it = solver.solve(*w.vector(), b);

  dolfin::cout << "Number of iterations (convergence in preconditioned norm): "
               << num_it << dolfin::endl;

  ISDestroy(&is[0]);
  ISDestroy(&is[1]);
}

#endif
